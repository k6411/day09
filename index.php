<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <h1 class="header">Trắc nghiệm</h1>
            <div class="wrapper-question">
                <div class="page page-1">
                    <h3>Page 1/2</h3>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi sinh năm nào?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">1992</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">1987</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">1989</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">1991</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi sinh ra ở đâu?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">Argentina</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">Brazil</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">Việt Nam</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">England</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi đã ghi bao nhiêu bàn?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">788</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">782</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">793</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">799</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi đang đá cho clb nào?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">Barcalona</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">Paris-Giemain</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">Liverpool</label>
                            </div>
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">Manchester City</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="next"><span>Tiếp theo</span></div>
                </div>
                    <div class="page page-2" style="display: none;">
                    <h3>Page 2/2</h3>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi sinh năm nào?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">1992</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">1987</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">1989</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">1991</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi sinh ra ở đâu?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">Argentina</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">Brazil</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">Việt Nam</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">England</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi đã ghi bao nhiêu bàn?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">788</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">782</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">793</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">799</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi đang đá cho clb nào?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">Barcalona</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">Paris-Giemain</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">Liverpool</label>
                            </div>
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">Manchester City</label>
                            </div>
                            </form>
                        </div>
                        <div class="question">
                        <div class="ask">
                            <p class="text-ask">
                                <span class="question-content"></span>
                                Messi đang đá cho clb nào?
                            </p>
                        </div>
                        <div class="answer">
                            <form action="" class="answerForm">
                            <div class="input-ans">
                                <input type="radio" name="answer" value="1" id="answer1">
                                <label class="form-label">Barcalona</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="2" id="answer2">
                                <label class="form-label">Paris-Giemain</label>
                            </div>
                            <div class="input-ans">
                                <input type="radio" name="answer" value="3" id="answer3">
                                <label class="form-label">Liverpool</label>
                            </div>
                                <input type="radio" name="answer" value="4" id="answer4">
                                <label class="form-label">Manchester City</label>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="page-btn">
                        <div class="back">Quay lại trang trước</div>
                        <div class="submit-btn">Nộp bài</div>
                    </div>
                    </div>
            </div>
        </div>
        <div id="result"></div>
    </div>
    <script>
        var nextPageBtns = document.querySelectorAll('.next');
        var backBtn = document.querySelector('.back');
        var submitBtn = document.querySelector('.submit-btn');
        var page1 = document.querySelector('.page-1');
        var page2 = document.querySelector('.page-2');
        var answerForms = document.querySelectorAll('.answerForm');
        console.log(answerForms)

        submitBtn.onclick = function() {
            for (i = 0; i < answers.length; i++) {
                if (answers[i].checked)
                    document.getElementById("result").innerHTML = "answer: " + answers[i].value;
            }
        }

        nextPageBtns.forEach(btn => {
            btn.onclick = function() {
                page1.style.display = 'none';
                page2.style.display = 'block';
            }
        })
        backBtn.onclick = function() {
            page2.style.display = 'none';
            page1.style.display = 'block';
        }
    </script>
</body>
</html>